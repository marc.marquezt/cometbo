/**
 * This template is a production ready boilerplate for developing with `CheerioCrawler`.
 * Use this to bootstrap your projects using the most up-to-date code.
 * If you're looking for examples or want to learn more, see README.
 */

const Apify = require("apify");
const { handleStart, handleList, handleDetail } = require("./src/routes");

const {
    utils: { log },
} = Apify;

Apify.main(async () => {
    const { startUrls } = await Apify.getInput();
    log.setLevel(log.LEVELS.DEBUG);
    const requestList = await Apify.openRequestList("start-urls", startUrls);
    const requestQueue = await Apify.openRequestQueue();
    //const proxyConfiguration = await Apify.createProxyConfiguration();
    async function enqueueRequest(request) {
        return requestQueue.addRequest(request);
    }
    const handlePageFunction = async ({ $, request }) => {
        const { Manufacturer, ProductPage, Paginated } = request.userData;
        if (!Paginated) {
            //while (!flag) {
            let contador = 0;
            for (let i = 1; i < 3; i++) {
                let flag = $("ol.paginator.paginator__item__next.disabled");
                if (!flag) {
                    contador++;
                }
                console.log("for", i);
                const nextUrl = `${request.url}&p=${i}`;
                console.log(`Next page: ${nextUrl}`);

                const pageRequest = {
                    url: nextUrl,
                    userData: {
                        Manufacturer,
                        Paginated: true,
                    },
                };

                await enqueueRequest(pageRequest);
                //i++;
            }
            console.log("contador", contador);
        }
        let products = [];
        if (
            $(".row.row-eq-height.no-gutters>.col-12.col-sm-6.col-md-4")
                .length > 0
        ) {
            $(".row.row-eq-height.no-gutters>.col-12.col-sm-6.col-md-4").each(
                async (index, element) => {
                    let productName = $(element)
                        .find("h3.caption__product")
                        .text();

                    let productCategory = $(element)
                        .find("h4.caption__category")
                        .text();

                    let productPrice = $(element)
                        .find(".caption__price")
                        .text();
                    productPrice = productPrice.replace(",", ".");
                    productPrice = productPrice.replace("€", " ");
                    productPrice = parseFloat(productPrice);

                    let productUrl = $(element)
                        .find(".js-gtm-click")
                        .attr("href");

                    let productImage = $(element)
                        .find(".wrap-img__img")
                        .attr("src");

                    let productId = $(element)
                        .find("a.js-gtm-click")
                        .attr("data-gtm-tag");
                    productId = productId.split(":")[2];

                    let stock = $(element).find(
                        '[class="hover-layer__link-cart js-proxy-ignore js-gtm-click"]'
                    ).length;

                    let product = {
                        Manufacturer: Manufacturer,
                        ProductPrice: productPrice,
                        ProductUrl: productUrl,
                        ProductName: productName,
                        ImageUri: productImage,
                        ProductId: productId,
                        Stock: stock ? "InStock" : "OutOfStock",
                    };
                    products.push(product);
                }
            );
            console.log(products.length, request.userData);
            await Apify.pushData(products);
        }
    };

    const crawler = new Apify.CheerioCrawler({
        requestList,
        requestQueue,
        //proxyConfiguration,
        // Be nice to the websites.
        // Remove to unleash full power.
        maxConcurrency: 50,
        handlePageFunction,
    });

    log.info("Starting the crawl.");
    await crawler.run();
    log.info("Crawl finished.");
});
